var circle = document.querySelector("circle.primary");
var circleVal = document.querySelector("text.value");
var radius = circle.r.baseVal.value;
var circumference = radius * 2 * Math.PI;

circle.style.strokeDasharray = `${circumference} ${circumference}`;
circle.style.strokeDashoffset = `${circumference}`;

function setProgress(percent) {
	circleVal.innerHTML = percent;
	const offset = circumference - (percent / 100) * circumference;
	circle.style.strokeDashoffset = offset;
	switch (true) {
		case percent < 33:
			circle.style.stroke = "red";
			break;
		case percent < 66:
			circle.style.stroke = "orange";
			break;
		default:
			circle.style.stroke = "green";
	}
}

const input = document.querySelector("input");
setProgress(input.value);

input.addEventListener("change", function(e) {
	if (input.value < 101 && input.value > -1) {
		setProgress(input.value);
	}
});

// adding rangeSlider-js for nicer input

var sliderEl = document.getElementById("slider");
rangesliderJs.create(sliderEl);
